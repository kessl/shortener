# Simple serverless shortener

URL shortener with a simple FE & serverless BE in Next.js using Mongo.

You can get a free Mongo instance at [Mongo Atlas](https://www.mongodb.com/cloud/atlas) and deploy to [Vercel](https://vercel.com/import/project?template=https://gitlab.com/kessl/shortener). In Vercel settings, set the `MONGO_URI` env variable to point to your Mongo.

The serverless API is documented in [shortener.yaml](shortener.yaml).
