const path = require('path')

module.exports = phase => ({
  rewrites: async () => [
    {
      source: '/:slug',
      destination: '/api/links/:slug',
    },
  ],

  webpack: config => {
    config.module.rules.push({
      test: /\.svg$/,
      use: [
        {
          loader: '@svgr/webpack',
          options: {
            titleProp: true,
            svgoConfig: {
              plugins: [{ removeViewBox: false }],
            },
          },
        },
      ],
      include: path.resolve(__dirname, 'icons'),
    })

    return config
  },

  poweredByHeader: false,
})
