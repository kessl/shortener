import React, { ButtonHTMLAttributes } from 'react'
import classNames from 'classnames'
import Spinner from 'icons/spinner.svg'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  loading?: boolean
}

export const Button: React.FC<ButtonProps> = ({ children, onClick, loading, ...props }) => (
  <button
    className={classNames(
      'text-gray-900 border-2 border-gray-400 bg-gray-200 rounded px-15 py-10',
      props.disabled && 'cursor-not-allowed'
    )}
    onClick={!props.disabled ? onClick : undefined}
    {...props}
  >
    {loading ? <Spinner className="opacity-50 inline mx-20" width={25} height={25} /> : children}
  </button>
)
