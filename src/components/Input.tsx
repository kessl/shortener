import React, { ReactNode, InputHTMLAttributes, forwardRef } from 'react'
import AutosizeInput from 'react-input-autosize'
import { FieldError } from 'react-hook-form/dist/types/form'
import classNames from 'classnames'

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  name?: string
  label?: ReactNode
  prefix?: string
  minWidth?: number
  description?: React.ReactNode
  error?: FieldError
}

export const Input = forwardRef<HTMLInputElement, InputProps>(
  ({ label, prefix, description, error, ...props }, ref) => (
    <>
      <div className="text-gray-900 border-2 border-gray-400 rounded flex flex-col sm:flex-row">
        {label && (
          <label
            htmlFor={props.name}
            className="text-gray-800 sm:rounded-l px-15 py-10 bg-gray-200 cursor-pointer"
          >
            {label}
          </label>
        )}
        {prefix && (
          <div className="font-mono p-10 sm:border-r-2 border-gray-400 flex-shrink">{prefix}</div>
        )}
        <AutosizeInput
          id={props.name}
          inputClassName="bg-white font-mono rounded p-10 w-full"
          inputStyle={{ maxWidth: '75vw' }}
          inputRef={ref as any}
          {...props}
        />
      </div>
      {!error && description && (
        <div className="text-xs text-gray-800 text-right mt-2 self-end">{description}</div>
      )}
      {error && (
        <div className="text-xs text-red-600 text-right mt-2 self-end">{error.message}</div>
      )}
      <div className="mb-10 sm:mb-0" />
    </>
  )
)
