import React, { useEffect, useState } from 'react'
import { Input } from 'components'
import { isValidUrl } from 'utils'
import { Button } from './Button'
import DownArrow from 'icons/arrow-down.svg'
import { useForm } from 'react-hook-form'
import { useHostname } from 'utils'
import classNames from 'classnames'
import { LinkApiClient } from 'data'
import slugify from 'unique-slug'

interface ShortenerFormData {
  original: string
  slug: string
  error?: string
}

function isFieldName(str): str is keyof ShortenerFormData {
  return str === 'original' || str === 'slug'
}

export const ShortenerForm: React.FC = () => {
  const form = useForm<ShortenerFormData>({ mode: 'all' })
  const { register, watch, setValue, handleSubmit, formState, errors, setError, trigger } = form
  const { isValid, isSubmitted } = formState

  const [result, setResult] = useState<string>()

  const submitForm = handleSubmit(async ({ slug, original }) => {
    const res = await LinkApiClient.save(slug, original)

    if (res.status === 'ok') {
      setResult(res.url)
      return
    }

    setError(isFieldName(res.field) ? res.field : 'error', { type: 'server', message: res.message })
    return new Promise((_, reject) => reject)
  })

  const original = watch('original', '')
  const host = useHostname()

  useEffect(() => {
    // generate a random slug once a valid URL is entered
    if (original.length > 0 && isValidUrl(original)) {
      setValue('slug', slugify(original))
      trigger('slug')
    }
  }, [original])

  return (
    <form
      method="post"
      action="/api/links"
      className="flex flex-col justify-center items-center"
      onSubmit={submitForm}
    >
      <div className={classNames(isSubmitted && 'opacity-75')}>
        <Input
          name="original"
          label="URL"
          placeholder="https://"
          minWidth={250}
          ref={register({
            required: 'Please enter a URL',
            validate: value =>
              (value.length > 0 && !isValidUrl(value) && 'Please enter a valid URL') || true,
          })}
          autoComplete="off"
          description="URL to be shortened"
          error={errors.original}
          readOnly={isSubmitted}
        />
      </div>

      <div
        className={classNames(
          'flex flex-col justify-center items-center',
          !isValidUrl(original) && 'opacity-25 pointer-events-none'
        )}
      >
        <DownArrow width={64} height={64} className="mb-20 fill-current text-gray-300" />
        {!isSubmitted && (
          <div className="flex flex-col sm:flex-row">
            <div>
              <Input
                name="slug"
                label="Short URL"
                minWidth={100}
                prefix={`https://${host}/`}
                ref={register({
                  required: 'Please enter a slug',
                  pattern: {
                    value: /[A-Za-z0-9_\-]+/,
                    message: '/[A-Za-z0-9_-]+/ only',
                  },
                })}
                description="Choose a short url"
                error={errors.slug}
              />
            </div>
            <div className="w-12" />

            {!isSubmitted && (
              <div className="text-md flex mt-15 sm:mt-0 justify-center items-start">
                <Button type="submit" disabled={!isValid} loading={formState.isSubmitting}>
                  Shorten
                </Button>
              </div>
            )}
          </div>
        )}

        {formState.isSubmitted && (
          <Input label="Short URL" description="Shortened URL" value={result} readOnly />
        )}
      </div>

      {errors.error && (
        <div className="text-sm text-gray-800 mt-30 text-center">
          Sorry, there was an error:
          <pre className="my-20 text-red-600 bg-red-100 border-2 border-red-200 rounded-sm p-10">
            {errors.error.message}
          </pre>
          Please try <a href="">reloading the page</a>.
        </div>
      )}
    </form>
  )
}
