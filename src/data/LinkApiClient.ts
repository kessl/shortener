type RequestInitWithObjectBody = Omit<RequestInit, 'body'> & {
  body: {
    [key: string]: string
  }
}

async function request<T = Response>(
  input: RequestInfo,
  init?: RequestInitWithObjectBody
): Promise<T> {
  const res = await fetch(
    input,
    Object.assign(
      {
        headers: {
          'Content-Type': 'application/json',
        },
      },
      { ...init, body: JSON.stringify(init?.body) }
    )
  )
  return res.json()
}

interface SaveLinkResponse {
  status: 'ok'
  url: string
  message: string
}

interface ErrorResponse {
  status: 'error'
  field: string
  message: string
}

export const LinkApiClient = {
  save: (slug: string, original: string) =>
    request<SaveLinkResponse | ErrorResponse>('/api/links', {
      method: 'post',
      body: {
        slug,
        original,
      },
    }),

  // TODO
  checkAvailability: async (slug: string) => {},
}
