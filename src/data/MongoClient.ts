import mongoose from 'mongoose'
import * as models from 'data/models'
import { NextApiHandler, NextApiRequest, NextApiResponse } from 'next'

declare module 'http' {
  interface IncomingMessage {
    models: typeof models
  }
}

let pendingPromise: Promise<typeof mongoose> | null = null

export const withMongo = (fn: NextApiHandler) => async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const next = () => {
    req.models = models
    return fn(req, res)
  }

  if (mongoose.connection.readyState === mongoose.connection.states.connected) {
    return next()
  }

  if (pendingPromise) {
    await pendingPromise
    return next()
  }

  if (!process.env.MONGO_URI) {
    throw new Error('Undefined MONGO_URI env variable.')
  }

  pendingPromise = mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })

  try {
    await pendingPromise
  } finally {
    pendingPromise = null
  }

  next()
}
