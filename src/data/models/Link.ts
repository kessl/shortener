import mongoose, { Document } from 'mongoose'

interface LinkDocument extends Document {
  slug: string
  original: string
}

const LinkSchema = new mongoose.Schema<LinkDocument>({
  slug: {
    type: String,
    required: true,
    unique: true,
  },
  original: {
    type: String,
    required: true,
  },
})

export const Link =
  (mongoose.models?.Link as mongoose.Model<LinkDocument>) ??
  mongoose.model<LinkDocument>('Link', LinkSchema)
