import { AppProps } from 'next/app'
import React from 'react'
import 'utils/global.css'

const ShortenerApp = ({ Component, pageProps }: AppProps) => <Component {...pageProps} />

export default ShortenerApp
