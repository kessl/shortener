import { NextApiRequest, NextApiResponse } from 'next'
import { withMongo } from 'data'

const redirectToLink = async (req: NextApiRequest, res: NextApiResponse) => {
  const slug = req.query.slug as string
  const checkOnly = !!req.query.check

  try {
    const link = await req.models.Link.findOne({ slug }).exec()

    if (checkOnly) {
      return res.status(200).json({ status: 'ok', exists: !!link })
    }

    if (!link) {
      return res.status(404).json({ status: 'error', message: `No URL found with slug ${slug}` })
    }

    res.redirect(302, link.original)
  } catch (ex) {
    res.status(500).json({ status: 'error', message: 'Internal server error' })
  }
}

export default withMongo(redirectToLink)
