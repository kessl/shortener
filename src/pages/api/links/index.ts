import { NextApiRequest, NextApiResponse } from 'next'
import { withMongo } from 'data'
import { isValidSlug } from 'utils'
import { Link } from 'data/models'

const ERR_DUPLICATE_INDEX = 11000

const saveLink = async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'POST') {
    return res.status(405).json({ status: 'error', message: 'Method not allowed' })
  }

  const slug = req.body.slug
  const original = /https?:\/\//.test(req.body.original)
    ? req.body.original
    : 'https://' + req.body.original
  const url = `https://${req.headers.host}/${slug}`

  if (!isValidSlug(slug)) {
    return res
      .status(400)
      .json({ status: 'error', field: 'slug', message: `Invalid slug: not /[A-Za-z0-9_\-]+/` })
  }

  try {
    const link = new Link({ original, slug })
    await link.save()

    return res.json({
      status: 'ok',
      url,
      message: `Your new short URL is ${url}`,
    })
  } catch (ex) {
    if (ex.code === ERR_DUPLICATE_INDEX) {
      return res
        .status(400)
        .json({ status: 'error', field: 'slug', message: `The URL ${url} is already taken.` })
    }
    res.status(500).end({ status: 'error', message: 'Internal server error' })
  }
}

export default withMongo(saveLink)
