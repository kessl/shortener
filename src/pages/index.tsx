import React from 'react'
import { NextPage } from 'next'
import Head from 'next/head'
import { ShortenerForm } from 'components'

const ShortenPage: NextPage = () => (
  <main className="h-full flex flex-col items-center bg-gray-100">
    <Head>
      <link
        href="https://fonts.googleapis.com/css2?family=Fira+Code&display=swap"
        rel="stylesheet"
      />
    </Head>
    <div className="text-lg font-extrabold mt-1/10 mb-30">Shorten a URL</div>
    <ShortenerForm />
  </main>
)

export default ShortenPage
