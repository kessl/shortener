import { useEffect, useState } from 'react'

export function useHostname() {
  const [host, setHost] = useState<string | undefined>(undefined)
  useEffect(() => {
    setHost(window.location.host)
  }, [])

  return host
}
