export function isValidUrl(str: string) {
  let url: URL

  try {
    url = new URL(str)
  } catch (_) {
    try {
      url = new URL('https://' + str)
    } catch (_) {
      return false
    }
  }

  return url.protocol === 'http:' || url.protocol === 'https:'
}

export function isValidSlug(str: string) {
  return !/[^A-Za-z0-9_\-]/.test(str)
}
