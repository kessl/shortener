const sizes = {
  '0': '0',
  '1': '1px',
  '2': '2px',
  '3': '3px',
  '4': '4px',
  '5': '5px',
  '6': '6px',
  '7': '7px',
  '8': '8px',
  '9': '9px',
  '10': '10px',
  '12': '12px',
  '14': '14px',
  '15': '15px',
  '16': '16px',
  '18': '18px',
  '20': '20px',
  '25': '25px',
  '30': '30px',
  '45': '45px',
  '50': '50px',
  '60': '60px',
  '70': '70px',
  '75': '75px',
  '80': '80px',
  '90': '90px',
  '100': '100px',
  '120': '120px',
  '240': '240px',

  '1/10': '10%',
}

module.exports = {
  theme: {
    fontFamily: {
      mono: ['"Fira Code"', 'monospace'],
    },
    fontSize: {
      xs: ['0.64rem', '1.4'],
      sm: ['0.8rem', '1.4'],
      md: ['1rem', '1.4'],
      lg: ['1.563rem', '1.4'],
      xl: ['2.441rem', '1.4'],
    },
    spacing: sizes,
  },
  variants: {},
  plugins: [],
  purge: {
    content: ['./src/**/*.{ts,tsx}'],
  },
  future: {
    removeDeprecatedGapUtilities: true,
  },
}
